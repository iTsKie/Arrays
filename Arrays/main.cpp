Normal Array use


#include<iostream>
using namespace std;

int main(){

    int marks[] = {23, 45, 56, 89};
    cout<<marks[0]<<endl;
    cout<<marks[1]<<endl;
    cout<<marks[2]<<endl;
    cout<<marks[3]<<endl;

    return 0;
}






Another example program to declare an Array is


#include<iostream>
using namespace std;

int main(){


    int mathMarks[4];
       mathMarks[0] = 2278;
       mathMarks[1] = 738;
       mathMarks[2] = 378;
       mathMarks[3] = 578;

       cout<<"These are math marks"<<endl;
       cout<<mathMarks[0]<<endl;
       cout<<mathMarks[1]<<endl;
       cout<<mathMarks[2]<<endl;
       cout<<mathMarks[3]<<endl;

    return 0;
}




Another array example program with using For-Loop


#include<iostream>
using namespace std;

int main(){

    int marks[] = {23, 45, 56, 89};

    for (int i = 0; i < 4; i++)
    {
        cout<<"the marks "<<i<<" is = "<<marks[i]<<endl;
    }
        return 0;
}



Printing marks using array with the help of While loop


#include<iostream>
using namespace std;

int main(){

    int i =0;
    int marks[] = {23, 45, 56, 89};

    while (i<4) {
        cout<<"the marks "<<i<<" is = "<<marks[i]<<endl;
        i++;
    }



    return 0;
}








//Printing marks using array with the help of Do-While loop


#include<iostream>
using namespace std;

int main(){
    int i = 0;
    int marks[] = {23, 45, 56, 89};

    do {
        cout<<"the marks "<<i<<" is = "<<marks[i]<<endl;
        i++;
    } while (i<4);
}
